module gitlab.com/tslocum/basiclander

go 1.15

require (
	github.com/ByteArena/box2d v1.0.2
	github.com/gdamore/tcell/v2 v2.1.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/tslocum/cbind v0.1.4
	gitlab.com/tslocum/cview v1.5.3-0.20201215184006-1af0da7606b8
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
)
